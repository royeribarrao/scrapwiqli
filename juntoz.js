const cron = require('node-cron');
const puppeteer = require('puppeteer');
const axios = require('axios');

class Main {
    static async getPrecios() {
        const browser = await puppeteer.launch();
        const page = await browser.newPage();
        const productosEvaluacion = await axios.get("http://127.0.0.1:8000/wiqli/productos/scraping");
        const todosProductosEvaluacion = productosEvaluacion.data;
        const productosGenerados = [];
        for(let productoEvaluar of todosProductosEvaluacion){
            if(productoEvaluar.url_juntoz)
            {
                try {
                    await page.goto(productoEvaluar.url_juntoz);

                    const precioOnline = await page.evaluate(() => {
                        const price = document.querySelector('.product__price__current strong span').innerHTML;
                        return price;
                    });
                    const preciosRequest = {};
                    
                    preciosRequest.id = productoEvaluar.id;
                    let indicePrecioOnline = precioOnline.indexOf("x");
                    preciosRequest.precioOnline = precioOnline;
                    productosGenerados.push(preciosRequest);
                } catch (error) {
                    
                }
            }
            
        }
        console.log(productosGenerados);
        let res = await axios.post("http://127.0.0.1:8000/wiqli/scraping/crear-precios/juntoz", productosGenerados);
        let data = res.data;
        console.log(data);
        await browser.close();
    };
}

// cron.schedule('10 * * * *', ()=> {
//     Main.getPrecios();
// });
(async () => {
    
    const browser = await puppeteer.launch();
    const page = await browser.newPage();
    const productosEvaluacion = await axios.get("http://127.0.0.1:8000/wiqli/productos/scraping");
    const todosProductosEvaluacion = productosEvaluacion.data;
    const productosGenerados = [];
    for(let productoEvaluar of todosProductosEvaluacion){
        if(productoEvaluar.url_juntoz)
        {
            try {
                await page.goto(productoEvaluar.url_juntoz);

                const precioOnline = await page.evaluate(() => {
                    const price = document.querySelector('.product__price__current strong span').innerHTML;
                    return price;
                });
                const preciosRequest = {};
                
                preciosRequest.id = productoEvaluar.id;
                let indicePrecioOnline = precioOnline.indexOf("x");
                preciosRequest.precioOnline = precioOnline;
                productosGenerados.push(preciosRequest);
            } catch (error) {
                
            }
        }
        
    }
    console.log(productosGenerados);
    let res = await axios.post("http://127.0.0.1:8000/wiqli/scraping/crear-precios/juntoz", productosGenerados);
    let data = res.data;
    console.log(data);
    await browser.close();
})();