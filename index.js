const express = require('express');

const app = express();
const cron = require('node-cron');
const puppeteer = require('puppeteer');
const axios = require('axios');

app.get('/', (req, res) => 
    res.send('Hello world.')
);

class Main {
  static async getPreciosVea() {
    const browser = await puppeteer.launch();
    const page = await browser.newPage();
    const productosEvaluacion = await axios.get(`https://backend.wiqli.com/wiqli/productos/scraping`);
    const todosProductosEvaluacion = productosEvaluacion.data;
    
    const productosGenerados = [];
    for(let productoEvaluar of todosProductosEvaluacion){
      if(productoEvaluar.url_vea)
      {
        try{
          await page.goto(productoEvaluar.url_vea);

          const precioRegular = await page.evaluate(() => {
            const integer = document.querySelector('.ProductCard__price--regular .ProductCard__price__integer').innerHTML;
            const decimal = document.querySelector('.ProductCard__price--regular .ProductCard__price__decimal').innerHTML;
            return integer+decimal;
          });
          const precioOnline = await page.evaluate(() => {
            const integer = document.querySelector('.ProductCard__price--online .ProductCard__price__integer').innerHTML;
            const decimal = document.querySelector('.ProductCard__price--online .ProductCard__price__decimal').innerHTML;
            return integer+decimal;
          });

          const precioOh= await page.evaluate(() => {
            const integer = document.querySelector('.ProductCard__price--oh .ProductCard__price__integer').innerHTML;
            const decimal = document.querySelector('.ProductCard__price--oh .ProductCard__price__decimal').innerHTML;
            return integer+decimal;
          });
          const preciosRequest = {};
          
          preciosRequest.id = productoEvaluar.id;
          let indicePrecioOnline = precioOnline.indexOf("x");
          let indicePrecioRegular = precioRegular.indexOf("x");
          preciosRequest.precioOnline = indicePrecioOnline >= 0 ? precioOnline.substring(0, indicePrecioOnline - 1) : precioOnline;
          preciosRequest.precioRegular = indicePrecioRegular >= 0 ? precioRegular.substring(0, indicePrecioOnline - 1) : precioRegular;
          preciosRequest.precioOh = precioOh;
          productosGenerados.push(preciosRequest);
        }catch (error){

        }
      }
    }

    let res = await axios.post(`https://backend.wiqli.com/wiqli/scraping/crear-precios/vea`, productosGenerados);
    let data = res.data;
    console.log(data);
    await browser.close();
  };

  static async getPreciosTottus() {
    const browser = await puppeteer.launch();
    const page = await browser.newPage();
    const productosEvaluacion = await axios.get("https://backend.wiqli.com/wiqli/productos/scraping");
    const todosProductosEvaluacion = productosEvaluacion.data;
    const productosGenerados = [];
    for(let productoEvaluar of todosProductosEvaluacion){
      if(productoEvaluar.url_tottus)
      {
        try {
          await page.goto(productoEvaluar.url_tottus);

          const precioOnline = await page.evaluate(() => {
            //const price = document.querySelector('.cmrPrice').textContent;
            const price = document.querySelector('.cmrPrice').innerHTML;
            return price;
          });
          const preciosRequest = {};
          
          preciosRequest.id = productoEvaluar.id;
          let indicePrecioOnline = precioOnline.indexOf("<");
          preciosRequest.precioOnline = indicePrecioOnline >= 0 ? (precioOnline.substring(0, indicePrecioOnline)).substring(3) : precioOnline.substring(3);
          //preciosRequest.precioOnline = precioOnline;
          productosGenerados.push(preciosRequest);
        } catch (error) {
            
        }
      }
        
    }
    let res = await axios.post("https://backend.wiqli.com/wiqli/scraping/crear-precios/tottus", productosGenerados);
    let data = res.data;
    console.log(data);
    await browser.close();
  };

  static async getPreciosWong() {
    const browser = await puppeteer.launch();
    const page = await browser.newPage();
    const productosEvaluacion = await axios.get("https://backend.wiqli.com/wiqli/productos/scraping");
    const todosProductosEvaluacion = productosEvaluacion.data;
    const productosGenerados = [];
    for(let productoEvaluar of todosProductosEvaluacion){
      if(productoEvaluar.url_wong)
      {
        try {
          await page.goto(productoEvaluar.url_wong);

          const precioOnline = await page.evaluate(() => {
            const integer = document.querySelector('.vtex-product-price-1-x-sellingPriceValue .vtex-product-price-1-x-currencyInteger').innerHTML;
            const decimal = document.querySelector('.vtex-product-price-1-x-sellingPriceValue .vtex-product-price-1-x-currencyFraction').innerHTML;
            return integer + '.' + decimal;
          });
          const preciosRequest = {};
          
          preciosRequest.id = productoEvaluar.id;
          preciosRequest.precioOnline = precioOnline;
          productosGenerados.push(preciosRequest);
        } catch (error) {
            
        }
      }
        
    }
    let res = await axios.post("https://backend.wiqli.com/wiqli/scraping/crear-precios/wong", productosGenerados);
    let data = res.data;
    console.log(data);

    await browser.close();
  };
}

cron.schedule('* 18 * * *', ()=> {
    Main.getPreciosVea();
    Main.getPreciosTottus();
    Main.getPreciosWong();
});
app.listen(3000);
console.log('server on port 3000');