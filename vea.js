const puppeteer = require('puppeteer');
const axios = require('axios');

(async () => {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  const productosEvaluacion = await axios.get(`https://backend.wiqli.com/wiqli/productos/scraping`);
  const todosProductosEvaluacion = productosEvaluacion.data;
  
  const productosGenerados = [];
  for(let productoEvaluar of todosProductosEvaluacion){
    if(productoEvaluar.url_vea)
    {
      try{
        await page.goto(productoEvaluar.url_vea);

        const precioRegular = await page.evaluate(() => {
          const integer = document.querySelector('.ProductCard__price--regular .ProductCard__price__integer').innerHTML;
          const decimal = document.querySelector('.ProductCard__price--regular .ProductCard__price__decimal').innerHTML;
          return integer+decimal;
        });
        const precioOnline = await page.evaluate(() => {
          const integer = document.querySelector('.ProductCard__price--online .ProductCard__price__integer').innerHTML;
          const decimal = document.querySelector('.ProductCard__price--online .ProductCard__price__decimal').innerHTML;
          return integer+decimal;
        });

        const precioOh= await page.evaluate(() => {
          const integer = document.querySelector('.ProductCard__price--oh .ProductCard__price__integer').innerHTML;
          const decimal = document.querySelector('.ProductCard__price--oh .ProductCard__price__decimal').innerHTML;
          return integer+decimal;
        });
        const preciosRequest = {};
        
        preciosRequest.id = productoEvaluar.id;
        let indicePrecioOnline = precioOnline.indexOf("x");
        let indicePrecioRegular = precioRegular.indexOf("x");
        preciosRequest.precioOnline = indicePrecioOnline >= 0 ? precioOnline.substring(0, indicePrecioOnline - 1) : precioOnline;
        preciosRequest.precioRegular = indicePrecioRegular >= 0 ? precioRegular.substring(0, indicePrecioOnline - 1) : precioRegular;
        preciosRequest.precioOh = precioOh;
        productosGenerados.push(preciosRequest);
      }catch (error){

      }
    }
  }

  let res = await axios.post(`https://backend.wiqli.com/wiqli/scraping/crear-precios/vea`, productosGenerados);
  let data = res.data;
  console.log(data);
  await browser.close();
})();