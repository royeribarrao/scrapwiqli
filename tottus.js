const cron = require('node-cron');
const puppeteer = require('puppeteer');
const axios = require('axios');

(async () => {
    const browser = await puppeteer.launch();
    const page = await browser.newPage();
    const productosEvaluacion = await axios.get("https://backend.wiqli.com/wiqli/productos/scraping");
    const todosProductosEvaluacion = productosEvaluacion.data;
    const productosGenerados = [];
    for(let productoEvaluar of todosProductosEvaluacion){
      if(productoEvaluar.url_tottus)
      {
        try {
          await page.goto(productoEvaluar.url_tottus);

          const precioOnline = await page.evaluate(() => {
            //const price = document.querySelector('.cmrPrice').textContent;
            const price = document.querySelector('.cmrPrice').innerHTML;
            return price;
          });
          const preciosRequest = {};
          
          preciosRequest.id = productoEvaluar.id;
          let indicePrecioOnline = precioOnline.indexOf("<");
          preciosRequest.precioOnline = indicePrecioOnline >= 0 ? (precioOnline.substring(0, indicePrecioOnline)).substring(3) : precioOnline.substring(3);
          //preciosRequest.precioOnline = precioOnline;
          productosGenerados.push(preciosRequest);
        } catch (error) {
            
        }
      }
        
    }
    let res = await axios.post("https://backend.wiqli.com/wiqli/scraping/crear-precios/tottus", productosGenerados);
    let data = res.data;
    console.log(data);
    await browser.close();
})();