const puppeteer = require('puppeteer');
const axios = require('axios');

(async () => {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  const productosEvaluacion = await axios.get("https://backend.wiqli.com/wiqli/productos/scraping");
  const todosProductosEvaluacion = productosEvaluacion.data;
  const productosGenerados = [];
  for(let productoEvaluar of todosProductosEvaluacion){
    if(productoEvaluar.url_wong)
    {
      try {
        await page.goto(productoEvaluar.url_wong);

        const precioOnline = await page.evaluate(() => {
          const integer = document.querySelector('.vtex-product-price-1-x-sellingPriceValue .vtex-product-price-1-x-currencyInteger').innerHTML;
          const decimal = document.querySelector('.vtex-product-price-1-x-sellingPriceValue .vtex-product-price-1-x-currencyFraction').innerHTML;
          return integer + '.' + decimal;
        });
        const preciosRequest = {};
        
        preciosRequest.id = productoEvaluar.id;
        preciosRequest.precioOnline = precioOnline;
        productosGenerados.push(preciosRequest);
      } catch (error) {
          
      }
    }
      
  }
  let res = await axios.post("https://backend.wiqli.com/wiqli/scraping/crear-precios/wong", productosGenerados);
  let data = res.data;
  console.log(data);

  await browser.close();
})();